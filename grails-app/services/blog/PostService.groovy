package blog

import grails.gorm.transactions.Transactional

@Transactional
class PostService {
    Post findById(Long id) {
        return Post.where {
            id == id
        }.find()
    }

    List<Post> getPosts() {
        return Post.list()
    }

    List<Post> getPublishedPosts() {
        return Post.where { publishedAt != null }.list([max: 10, sort: 'publishedAt', order: 'desc'])
    }

    Post createPost(Post post) {
        post.save()
        return post
    }

    Post updatePost(Post post) {
        post.save()
        return post
    }

    boolean deletePost(Post post){
        post.delete()
        return true
    }
}
