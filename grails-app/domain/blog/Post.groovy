package blog

class Post {
    String title
    String body
    Date publishedAt

    static constraints = {
        title blank:false
        body blank:false

        publishedAt nullable: true
    }
}
