package blog

class UrlMappings {

    static mappings = {
//        "/$controller/$action?/$id?(.$format)?"{
//            constraints {
//                // apply constraints here
//            }
//        }

        group "/api/v1", {
            get "/posts" (controller:"post", action:"index")
            post "/posts" (controller:"post", action:"save")
            get "/posts/$id" (controller:"post", action:"show")
            put "/posts/$id" (controller:"post", action:"update")
            delete "/posts/$id" (controller:"post", action:"delete")
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
