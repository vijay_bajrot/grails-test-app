package blog

import grails.converters.JSON

class PostController {

    final private PostService postService;

    PostController(PostService postService){
        this.postService = postService
    }

    def index() {
        // Get published from query params
        def published = params.published ? true: false

        // Get data from database using services
        List<Post> posts;

        if (published) {
            posts = this.postService.getPublishedPosts()
        } else {
            posts = this.postService.getPosts()
        }

        // Return json response
        //return render(posts as JSON)
        return render(view: 'list', model: [posts: posts])
    }

    def show(Long id){
        def post = postService.findById(id)
        //return  render(post as JSON)

        return render(view: 'show', model: [post: post])
    }


    /** Create a post **/
    def save(Post post) {

        // Check for validation
        if(post.hasErrors()) {
            response.status = 400
            return render(post.errors as JSON)
        }

        // Save to DB
        this.postService.createPost(post)

        // Send success response
        HashMap<String, String> successResponse = new HashMap<>()
        successResponse.put("message", "Post created!")

        return render(successResponse as JSON)
    }

    /** Update a post **/
    def update(Post post) {
        if(!post || !post.id) {
            return render(status:404)
        }

        // Check for validation
        if(post.hasErrors()) {
            response.status = 400
            return render(post.errors as JSON)
        }

        // Save post
        this.postService.updatePost(post)

        // Send success response
        HashMap<String, String> successResponse = new HashMap<>()
        successResponse.put("message", "Post updated!")
        successResponse.put("post", post)

        response.status = 200
        return render(successResponse as JSON)
    }

    /** Delete a post **/
    def delete(Post post) {
        if(!post || !post.id) {
            return render(status:404)
        }

        // Remove post
        this.postService.deletePost(post)

        // Send success response
        HashMap<String, String> successResponse = new HashMap<>()
        successResponse.put("message", "Post deleted!")

        response.status = 200
        return render(successResponse as JSON)
    }
}
